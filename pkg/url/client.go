package url

import (
	"api-gateway/pkg/config"
	"api-gateway/pkg/url/pb"
	"fmt"

	"google.golang.org/grpc"
)

type UrlClient struct {
	Client pb.UrlServiceClient
}

func InitUrlClient(c *config.Config) pb.UrlServiceClient {
	cc, err := grpc.Dial(c.UrlSvc, grpc.WithInsecure())
	if err != nil {
		fmt.Println("Could not connect:", err)
	}
	
	return pb.NewUrlServiceClient(cc)
}

package routes

import (
	"api-gateway/pkg/url/pb"
	"context"
	"net/http"

	"github.com/gin-gonic/gin"
)

type CreateUrlRequestBody struct {
	OriginalUrl string `json:"original_url"`
	ShortenUrl  string `json:"shorten_url"`
}

func CreateUrl(ctx *gin.Context, c pb.UrlServiceClient) {
	u := CreateUrlRequestBody{}

	if err := ctx.BindJSON(&u); err != nil {
		ctx.AbortWithError(http.StatusBadRequest, err)
		return
	}

	res, err := c.CreateUrl(context.Background(), &pb.CreateUrlRequest{
		OriginalUrl: u.OriginalUrl,
		ShortenUrl:  u.ShortenUrl,
	})
	if err != nil {
		ctx.AbortWithError(http.StatusBadGateway, err)
		return
	}

	ctx.JSON(http.StatusCreated, &res)
}

package routes

import (
	"api-gateway/pkg/url/pb"
	"context"
	"net/http"

	"github.com/gin-gonic/gin"
)

type RedirectRequestBody struct {
	ShortenUrl string `json:"shorten_url"`
}

func Redirect(ctx *gin.Context, c pb.UrlServiceClient) {
	shortenURL := ctx.Param("shortenUrl")
	if shortenURL == "" {
		ctx.String(http.StatusBadRequest, "shortenUrl parameter is missing")
		return
	}

	res, err := c.Redirect(context.Background(), &pb.RedirectRequest{
		ShortenUrl: shortenURL,
	})
	if err != nil {
		ctx.AbortWithError(http.StatusBadGateway, err)
		return
	}

	ctx.Redirect(http.StatusFound, res.OriginalUrl)
}

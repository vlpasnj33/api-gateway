package url

import (
	"api-gateway/pkg/auth"
	"api-gateway/pkg/config"
	"api-gateway/pkg/url/routes"

	"github.com/gin-gonic/gin"
)

func UrlRoutes(r *gin.Engine, c *config.Config, authSvc *auth.AuthClient) *UrlClient {
	a := auth.InitAuthMiddleware(authSvc)

	svc := &UrlClient{
		Client: InitUrlClient(c),
	}

	routes := r.Group("/url/v1")

	routes.GET("/:shortenUrl", svc.Redirect)

	routes.Use(a.AuthRequired)
	{
		routes.POST("/create", svc.CreateUrl)
	}


	return svc
}

func (svc *UrlClient) CreateUrl(ctx *gin.Context) {
	routes.CreateUrl(ctx, svc.Client)
}

func (svc *UrlClient) Redirect(ctx *gin.Context) {
	routes.Redirect(ctx, svc.Client)
}

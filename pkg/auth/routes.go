package auth

import (
	"api-gateway/pkg/auth/routes"
	"api-gateway/pkg/config"

	"github.com/gin-gonic/gin"
)

func AuthRoutes(r *gin.Engine, c *config.Config) *AuthClient {
	svc := &AuthClient{
		Client: InitServiceClient(c),
	}

	routes := r.Group("/auth")
	{
		routes.POST("/register", svc.Register)
		routes.POST("/login", svc.Login)
	} 

	return svc
}

func (svc *AuthClient) Register(ctx *gin.Context) {
    routes.Register(ctx, svc.Client)
}

func (svc *AuthClient) Login(ctx *gin.Context) {
    routes.Login(ctx, svc.Client)
}
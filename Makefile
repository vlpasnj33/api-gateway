server:
	go run cmd/main.go 

proto_auth:
	protoc --go_out=. --go_opt=paths=source_relative \
    	   --go-grpc_out=. --go-grpc_opt=paths=source_relative pkg/auth/pb/auth.proto

proto_url:
	protoc --go_out=. --go_opt=paths=source_relative \
    	   --go-grpc_out=. --go-grpc_opt=paths=source_relative pkg/url/pb/url.proto
